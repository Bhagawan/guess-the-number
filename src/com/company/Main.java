package com.company;

import java.lang.Math;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int number = (int)(Math.random() * 101);
        String input;
        int guess;
        Scanner scan = new Scanner(System.in);
        int work = 0;
        System.out.println ("Угадайте число от 0 до 100. Внимание: У вас только 10 попыток");

        while(work < 10) {
            System.out.println("Введите  число:");
            input = scan.next();
            try{
                guess= Integer.parseInt(input);
                if (guess < 0 || guess > 100) System.out.println("Число должно быть в диапозоне 0-100.");
                else if (guess < number){
                    System.out.println("Введеное число меньше задуманного.");
                    work++;
                }
                else if (guess > number) {
                    System.out.println("Введеное число больше задуманного.");
                    work++;
                }
                else if (guess == number) {
                    System.out.println("Поздравляю!! Вы угадали.");
                    work=10;
                }
            } catch (NumberFormatException nfe){
                System.out.println("Это не число");
            }

            if (work == 10){
                System.out.println("Хотите сыграть снова [Да]/[Нет]");
                while(true) {
                    String answer = scan.next();
                    if(answer.equals("Да")){
                        number = (int)(Math.random() * 101);
                        work = 0;
                        break;
                    }
                    else if(answer.equals("Нет")){
                        break;
                    }
                }
            }
        }
        scan.close();
    }
}
